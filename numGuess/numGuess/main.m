//
//  main.m
//  numGuess
//
//  Created by Raul Gil on 9/10/12.
//  Copyright (c) 2012 Raul Gil. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "numGuessAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([numGuessAppDelegate class]));
    }
}
