//
//  numGuessViewController.h
//  numGuess
//
//  Created by Raul Gil on 9/10/12.
//  Copyright (c) 2012 Raul Gil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface numGuessViewController : UIViewController
{
    NSString *myWord;
    NSString *guess;
    UIAlertView *message;
    UIAlertView *playAgainMessage;
    
}
@property (weak, nonatomic) IBOutlet UILabel *intro1stLine;
@property (weak, nonatomic) IBOutlet UILabel *intro2ndLine;
@property (weak, nonatomic) IBOutlet UIButton *gotNumberButton;
@property (weak, nonatomic) IBOutlet UIView *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *higherButton;
@property (weak, nonatomic) IBOutlet UIButton *lowerButton;

- (id) init;
@end
