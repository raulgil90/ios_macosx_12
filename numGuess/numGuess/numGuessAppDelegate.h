//
//  numGuessAppDelegate.h
//  numGuess
//
//  Created by Raul Gil on 9/10/12.
//  Copyright (c) 2012 Raul Gil. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface numGuessAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
