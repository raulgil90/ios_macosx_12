//
//  numGuessViewController.m
//  numGuess
//
//  Created by Raul Gil on 9/10/12.
//  Copyright (c) 2012 Raul Gil. All rights reserved.
//

#import "numGuessViewController.h"

@interface numGuessViewController ()

@end

@implementation numGuessViewController
@synthesize intro1stLine;
@synthesize intro2ndLine;
@synthesize gotNumberButton;
@synthesize yesButton;
@synthesize higherButton;
@synthesize lowerButton;

BOOL gameStart = NO;
NSInteger number = 500;
NSInteger rangeMax = 1000;
NSInteger rangeMin = 1;
NSInteger midpoint = 500;
NSInteger guesses = 0;

- (id) init {
    self = [super init];
    [yesButton setHidden:YES];
    [higherButton setHidden:YES];
    [lowerButton setHidden:YES];
    return self;
}

- (IBAction)gotNumButtonPressed:(id)sender {
    // ---- Hide the text ---- //
    [intro1stLine setHidden:YES];
    [gotNumberButton setHidden:YES];
    
    // ---- Allocate and initialize alert and text objects ---- //
    myWord = [[NSString alloc] init];
    guess = [[NSString alloc]init];
    
    
    // ---- Declare the game has started ---- //
    gameStart = YES;
    
    // ---- go play ---- //
    [self startGame];
}

- (IBAction)highPressed:(id)sender {
    if(!gameStart) {
        [message show];
    }// endif the game hasn't started, alert the user
    else {
        midpoint = [self getNewMidpoint:midpoint andIsLowerBound:NO];
        [self startGame];
    }
    
}

- (IBAction)lowerPressed:(id)sender {
    if(!gameStart) {
        [message show];
    }// endif the game hasn't started, alert the user
    else {
        // get the new number (midpoint)
        midpoint = [self getNewMidpoint:midpoint andIsLowerBound:YES];
        
        // loop over back into the game
        [self startGame];
    }// else, continue with game
    
}// end lowerPressed:(id)sender method

- (IBAction)yesButtonPressed:(id)sender {
    if (!gameStart) {
        [message show];
    }// endif the game hasn't started, alert the user
    else {
        [intro1stLine setText:@"Yes I win!"];
        [playAgainMessage show];
    }// else, the "AI" should win
    
}// end yesButtonPressed:(id)sender method

- (IBAction)startGame {
    
    // ---- Un-hide all the needed items ---- //
    [yesButton setHidden:NO];
    [higherButton setHidden:NO];
    [lowerButton setHidden:NO];
    [intro1stLine setHidden:NO];
    [intro2ndLine setHidden:NO];
    
    // ---- Display any necessary text ---- //
    guess = [NSString stringWithFormat:@"Guesses: %d", guesses];
    myWord = [NSString stringWithFormat:@"Is your number: %d",midpoint];
    guesses++;
    
    intro1stLine.text = guess;
    intro2ndLine.text = myWord;

}// end (IBAction)startGame method



- (NSInteger) getNewMidpoint:(NSInteger)oldMidpoint andIsLowerBound:(BOOL)lowerBound {
    
    if (lowerBound) {
        rangeMax = oldMidpoint;
        midpoint = (rangeMin + rangeMax) / 2;
    }// endif number is lower than current midpoint
    else {
        rangeMin = oldMidpoint;
        midpoint = (rangeMin + rangeMax) / 2;
    }// end else
    
    return midpoint;
    
}// end getNewMidpoint:(NSInteger)oldMidpoint andIsLowerBound:(BOOL)lowerBound method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex) {
        // cancel button pressed
    }
    else if (buttonIndex == alertView.firstOtherButtonIndex) {
        guesses = 0;
        rangeMax = 1000;
        rangeMin = 1;
        midpoint = 500;
        gameStart = NO;
        [intro1stLine setText:@"Think of a number from 1-1000"];
        [intro2ndLine setText:@"Bet I can guess it in 10 tries or less"];
        [gotNumberButton setHidden:NO];
    }
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   message = [[UIAlertView alloc] initWithTitle:@"Error!" message:@"You have not chosen a number" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    playAgainMessage = [[UIAlertView alloc] initWithTitle:@"Play" message:@"Play Again?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setIntro1stLine:nil];
    [self setIntro2ndLine:nil];
    [self setGotNumberButton:nil];
    [self setYesButton:nil];
    [self setHigherButton:nil];
    [self setLowerButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
